// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require ('../models/Course')
	const bcrypt = require('bcrypt');
	const auth = require('../auth'); 

// [SECTION] Functionalities [Create]
	module.exports.registerUser = (data) => {
		let fName = data.firstName;		
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let mobil = data.mobileNo;

		let newUser = new User({
			firstName : fName,
			lastName : lName,
			email : email,
			password : bcrypt.hashSync(passW, 10) ,
			mobileNo : mobil
		});

		return newUser.save().then((user,err) => {
			if (user) {
				return user;
			} else {
				return false;
			};
		});
	};
	
	// Email checker
	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0 ) {
				return 'Email already exists';
			} else {
				return 'Email is Still Available';
			};
		});
	};

	// Login User
	 module.exports.loginUser = (reqBody) => {
     return User.findOne({email: reqBody.email}).then(result => {
        let uPassW = reqBody.password; 
        if (result === null) {
          return false;
        	} else {
        		let passW = result.password;
           		const isMatched = bcrypt.compareSync(uPassW, passW); 
           		if (isMatched) {
             		let dataNiUser = result.toObject();
             		return {accessToken: auth.createAccessToken(dataNiUser)};
           		} else {
             		return false; 
           		}
       		};
    	});
 	 };

 	

 	 module.exports.enroll = async (data) => {
 	 		let id = data.userId;
 	 		let course = data.courseId;

 	 		 let isCourseUpdated = await Course.findById(course).then(course => {
 	 				const userFound = course.enrollees.find(courseObj => courseObj.userId == id);
 	 					if(!userFound) {
 	 							course.enrollees.push({userId: id});
 	 							return course.save().then((save, err) => {
 	 								if (err) {
 	 									return false;
 	 								} else {
 	 									return true;
 	 								};
 	 						});
 	 					} else {
 	 							return false;
 	 				}
 	 			});

 	 		let isUserUpdated = await User.findById(id).then(user => {
 	 			const courseFound = user.enrollments.find(courseObj => courseObj.courseId == course);
 	 				if(!courseFound) {
 	 					user.enrollments.push({courseId: course});
 	 					return user.save().then((save, error) => {
 	 						if (error) {
 	 							return false;
 	 						} else {
 	 							return true;
 	 						};
 	 					});
 	 				} else {
 	 						return false;
 	 				}
 	 		});

 	 
 	 			if (isUserUpdated && isCourseUpdated) {
 	 				return 'Enrollment Successs'
 	 			} else {
 	 				return 'Enrollment Failed, Go to Registrar'
 	 			};
 	 		};



// [SECTION] Functionalities [Retrieve]
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};



// [SECTION] Functionalities [Update]
	module.exports.setAsAdmin = (userId) => {
			let updates = {
				isAdmin: true
			}
		return User.findByIdAndUpdate(userId, updates).then((admin, err)=> {
			if (admin) {
				return true
			} else {
				return 'Updates Failed to implement';
			}
		});
	};

	//Set User as Non-Admin 
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates). then((user, err) => {
			if (user) {
				return true 
			} else {
				return 'Failed to update user'
			};
		});
	};






