// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Routes-[POST]	
	route.post('/register', (req, res) => {
		let userData = req.body;
		controller.registerUser(userData).then(outcome => {
			res.send(outcome);
		});
	});

	route.post('/check-email',(req, res) => {
		controller.checkEmailExists(req.body).then(outcome => {
			res.send(outcome);
		});
	});
	
	// [Login User]=? Static Route
	route.post('/login', (req, res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome)
		});
	});

	// Authenticated User Course Enrollment
	route.post('/enroll', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id//user id
		let isAdmin = payload.isAdmin;
		let subjectId = req.body.courseId;

		let data = {
			userId: userId,
			courseId: subjectId
		};

		if (!isAdmin) {
			controller.enroll(data).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send('Dont Allow user to enroll');
		};
	})



// [SECTION] Routes-[GET]
	route.get('/details', auth.verify ,(req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		});
	});
// [SECTION] Routes-[UPDATE] Admin only
	route.put('/:userId/set-as-admin',auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin
		let id = req.params.userId;
		(isAdmin) ? controller.setAsAdmin(id).then(outcome => res.send(outcome))
		: res.send ('Unauthorized User');
	});

	// Set user as a non-admin role [DYNAMIC]
	route.put('/:userId/set-as-user', auth.verify ,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.userId;
		isAdmin ? controller.setAsNonAdmin(id).then(result => res.send(result))
		: res.send ('Unauthorized User');
	});

// [SECTION] Routes-[DEL]

// [SECTION] Expose Route Susten
	module.exports = route;