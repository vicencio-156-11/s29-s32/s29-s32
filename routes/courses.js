// [SECTION] Dependencies and modules
	const exp = require('express');
	const controller = require('../controllers/courses');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router()

// [SECTION]-[POST] Route (Admin only)
	route.post('/', auth.verify , (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body,
		}
		if (isAdmin) {
			controller.addCourse(data).then(outcome => {
			res.send(outcome)
			});
		} else {
			res.send('User Unauthorized to Proceed!')
		};	
	});
	
// [SECTION]-[GET] Route (Admin only)
	// Retrieve All Courses
	route.get('/all', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		isAdmin ? controller.getAllCourses().then(outcome =>
			res.send(outcome))
		:
		res.send('Unauthorized User');
	});

	// Retrieve ONLY Active Courses (both)
	route.get('/', (req, res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	// Retrive Single Course (both)
	route.get('/:id',(req, res) => {
		let data = req.params.id
		controller.getCourse(data).then(result => {
			res.send(result)
		});
	});

// [SECTION]-[PUT] Route (Admin)
	// Update Course
	route.put('/:courseId', auth.verify,(req, res) => {
		let params = req.params;
		let body = req.body;
		if (auth.decode(req.headers.authorization).isAdmin) {
			controller.updateCourse(params, body).then(outcome => {
			res.send(outcome);
			});
		} else {
			res.send('User Unauthorized');
		};
	});

	//Course Archive (Admin)
	route.put('/:courseId/archive',auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? 
				controller.archiveCourse(params).then(result => {
				res.send(result)
				})
			: 
				res.send('Unauthorized User'); 
	});	

	// Set course as active [DYNAMIC]
	route.put('/:courseId/set-as-non-active', (req, res) => {
		let id = req.params.courseId
		controller.setAsNonActive(id).then(result => {
			res.send(result);
		});
	});


// [SECTION]-[DEL] Route (admin)
	route.delete('/:courseId', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.courseId;
		(isAdmin) ? controller.deleteCourse(id).then(outcome =>
			res.send(outcome))
		: 
			res.send('Unauthorized User'); 
	});

// [SECTION] Export Route System
 module.exports = route;